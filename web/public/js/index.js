var PINBRAIN_API_URL = 'http://api.pinbrain.ru';
var pinbrainApp = angular.module('pinbrainApp', ['ngCookies']);
pinbrainApp.controller('authController', function ($scope, $cookies, $http) {
    $scope.needRegister = undefined;
    $scope.authorized = false;
    var user_info = $cookies.getObject("user_id");
    if (user_info){
        $scope.authorized = true;
        $scope.username = user_info.username;
        window.location = "/queries.html";
        return;
    }
    $scope.setRegisterRequired = function(val) {
        $scope.needRegister = val;
    };
    $scope.register = function (username, password) {
        username = username.toLowerCase();
        $http.get(PINBRAIN_API_URL+'/register', {params: {
            username: username,
            password: password
        }}).then(function (res) {
            console.log(res);
            if (res.status !== 200) return alert('Connection error');
            if (!res.data.result) {
                console.log('Authorization error:', res.data.error);
                if (res.data.error == 'This username already taken') {
                    return alert(res.data.error);
                } else {
                    return alert('Server error :(');
                }
            }
            $scope.username = username;
            $scope.authorized = true;
        });
    };
    $scope.login = function (username, password) {
        username = username.toLowerCase();
        $http.get(PINBRAIN_API_URL+'/login', {params: {
            username: username,
            password: password
        }}).then(function (res) {
            if (res.status !== 200) return alert('Connection error');
            if (!res.data.result) {
                if (res.data.error) {
                    console.log('Authorization error:', res.data.error);
                    return alert('Server error :(');
                } else {
                    return alert('Wrong username or password!');
                }
            }
            $scope.username = username;
            $scope.authorized = true;
            $cookies.putObject("user_id", {"id": res.data.user_id, "username": username});
            window.location = "/queries.html";
        });
    };
    $scope.logout = function () {
        $scope.authorized = false;
        $cookies.remove("user_id");
        window.location = "/";
    };
});