var PINBRAIN_API_URL = 'http://api.pinbrain.ru';
var pinbrainApp = angular.module('pinbrainApp', ['ngCookies']);
pinbrainApp.controller('authController', function ($scope, $cookies) {
    $scope.needRegister = undefined;
    $scope.authorized = false;
    var user_info = $cookies.getObject("user_id");
    if (user_info){
        $scope.authorized = true;
        $scope.username = user_info.username;
    } else {
        window.location = "/";
    }
    $scope.logout = function () {
        $scope.authorized = false;
        $cookies.remove("user_id");
        window.location = "/";
    };
}).controller('queryController', function ($scope, $http, $cookies) {
    $scope.responses = [];
    $scope.sendQuery = function () {
        console.log($cookies.getObject("user_id"));
        $http.get(PINBRAIN_API_URL+'/execute', {params: {
            user_id: $cookies.getObject("user_id").id,
            query: $scope.query
        }}).then(function (res) {
            console.log(res);
            if (res.status !== 200) return alert('Connection error');
            if (!res.data.result) {
                console.log('Query error.');
                return alert('Query error :(');
            }
            var value = res.data.value;
            while ($scope.responses.length) $scope.responses.pop();
            if (!value) return $scope.responses.push({type: 'string', value: 'Data saved successful'});
            if (value.length == 0) return $scope.responses.push({type: 'string', value: 'Relevant data wasn\'t found'});
            for (let i=0; i<value.length; ++i) $scope.responses.push(value[i]);
        });
    };
});