/**
 * Created by nickolai on 15.10.16.
 */
'use strict';
const mongo = require('./db');
const async = require('async');
const util = require('util');

function register(username, password, callback) {
    getUserName(username, (err, name) => {
            if (err) return callback(err);
            if (name) return callback('This username already taken');
            setUserName(username, password, (err) => {
                if (err) return callback(err);
                return callback();
            })
        });
}

function login(username, password, callback) {
    let db = null;
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (_db, cb) => {
            db = _db;
            let usersCollection = db.collection('users');
            usersCollection.findOne({username: username, password: password}, cb);
        }
    ], (err, object) => {
        if (!db) return callback(err);
        db.close(() => {callback(err, (object) ? object._id : undefined)});
    });
}

function getUserName(username, callback) {
    let db = null;
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (_db, cb) => {
            db = _db;
            let usersCollection = db.collection('users');
            usersCollection.findOne({username: username}, cb);
        }
    ], (err, object) => {
        if (!db) return callback(err);
        db.close(() => {callback(err, (object) ? object.username : undefined)});
    });
}

function setUserName(username, password, callback) {
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (db, cb) => {
            let usersCollection = db.collection('users');
            usersCollection.insertOne({
                username: username,
                password: password
            }, (err) => {cb(err, db);});
        },
        (db, cb) => db.close(cb)
    ], callback);
}

function findIdByText(uid, arr, callback) {
    let db = null;
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (_db, asyncCallback) => {
            db = _db;
            let i = 0;
            async.doWhilst(cb => {
                db.collection('data').findOne({
                    user_id: uid,
                    text: {$regex: arr[i]}
                }, (err, res) => {
                    if (err) return cb(err);
                    if (!res) {
                        i++;
                        return cb(null, false);
                    } else {
                        return cb(null, res);
                    }
                });
            }, res => !res && i<arr.length, asyncCallback);
        }
    ], (err, res) => {
        if (db) db.close();
        if (err) return callback(err);
        callback(null, res);
    });
}

function findObjectByField(uid, field, callback) {
    let db = null;
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (_db, cb) => {
            db = _db;
            let fnd = {user_id: uid};
            fnd[field.name] = field.value;
            db.collection('data').findOne(fnd, (err, res) => {
                if (err) return cb(err);
                if (!res) {
                    return cb(null, false);
                } else {
                    return cb(null, res);
                }
            });
        }
    ], (err, res) => {
        if (db) db.close();
        if (err) return callback(err);
        callback(null, res);
    });
}

function upsertData(uid, obj, phone, email, text, callback) {
    function aou(f, val) {
        if (obj[f] && !util.isArray(obj[f])) {
            if (obj[f] != val) obj[f] = [obj[f], val];
        } else if (obj[f]) {
            for (let i=0; i<obj[f].length; i++)
                if (obj[f][i] == val) return;
            obj[f].push(val);
        } else {
            obj[f] = val;
        }
    }
    obj = obj || {};
    obj.user_id = uid;
    if (phone) aou('phone', phone);
    if (email) aou('email', email);
    if (text) aou('text', text);
    let db = null;
    async.waterfall([
        cb => mongo.mongo_connect(cb),
        (_db, cb) => {
            db = _db;
            let collection = db.collection('data');
            if (obj._id) {
                collection.updateOne({_id: new mongo.ObjectID(obj._id)}, {$set: obj}, cb);
            } else {
                collection.insertOne(obj, cb);
            }
        }
    ], err => {
        if (db) db.close();
        callback(err);
    });
}

module.exports = {
    register,
    login,
    findIdByText,
    findObjectByField,
    upsertData
};