function searchPhone(str) {
    let x = /\+?\d[\d\(\)\-\s]+/.exec(str), res = '';
    if (x) {
        let s = x[0];
        for (let i=0; i<s.length; ++i) {
            let c = s.charAt(i);
            if (c=='+' && !res.length) res += c; else
            if ((c>='0') && (c <= '9')) res += c;
        }
    }
    return res;
}

function searchEmail(str) {
    let x = /[a-zA-Z\-_\.0-9]+@[a-zA-Z\-_\.0-9]+\.[a-zA-Z]+/.exec(str);
    return x ? x[0] : '';
}

function searchWords(str) {
    let s = str.replace(/[a-zA-Z\-_\.0-9]+@[a-zA-Z\-_\.0-9]+\.[a-zA-Z]+/, '');
    return s.split(/[ ,\.\/\\\*\+\|\?\!#\$%\^&\(\)\[\]\{\}]/).reduce((res, x) => x&&isNaN(x)?res.concat(x):res, []);
}

module.exports = {
    searchPhone,
    searchEmail,
    searchWords
};