const utils = require('./utils');
const util = require('util');
const model = require('../model');

module.exports = function (user_id, query, res) {
    let phone = utils.searchPhone(query);
    let email = utils.searchEmail(query);
    let words = utils.searchWords(query);
    let searchPhone = false;
    let searchEmail = false;
    for (let i = 0; i < words.length; ++i){
        if (words[i] == 'phone' || words[i] == 'телефон' || words[i] == 'тел' || words[i] == 'тел.') searchPhone = true;
        if (words[i] == 'email' || words[i] == 'почта') searchEmail = true;
    }
    words.reduce((res, r) => r.length>3 ?
            res.concat(r.length<6 ? r : r.substr(0, r.length-2)) : res, []);

    let handler = model.findIdByText;
    let fieldParam = words;
    if (email){
        handler = model.findObjectByField;
        fieldParam = {email: email};
    } else if (phone){
        handler = model.findObjectByField;
        fieldParam = {phone: phone};
    }
    handler(user_id, fieldParam, (err, obj) => {
        if (err) return res.json({
            result: false,
            error: err.message
        });
        let result = {result: true};
        result['value'] = [];
        if (obj.email && !searchPhone){
            if (util.isArray(obj.email)){
                for (let i = 0; i < obj.email.length; ++i){
                    result.value.push({type: 'link', text: obj.email[i], url: util.format('mailto:%s',obj.email[i])});
                }
            } else {
                result.value.push({type: 'link', text: obj.email, url: util.format('mailto:%s',obj.email)})
            }
        }
        if (obj.phone && !searchEmail){
            if (util.isArray(obj.phone)){
                for (let i = 0; i < obj.phone.length; ++i){
                    result.value.push({type: 'link', text: obj.phone[i], url: util.format('tel:%s',obj.phone[i])});
                    result.value.push({type: 'link', text: util.format('Voximplant: %s', obj.phone[i]), url: util.format('http://api.pinbrain.ru/call.html#number=%s',obj.phone[i])});
                }
            } else {
                result.value.push({type: 'link', text: obj.phone, url: util.format('tel:%s',obj.phone)})
                result.value.push({type: 'link', text: util.format('Voximplant: %s', obj.phone), url: util.format('http://api.pinbrain.ru/call.html#number=%s',obj.phone)});
            }
        }
        if (obj.text && !searchPhone && !searchEmail){
            if (util.isArray(obj.text)){
                for (let i = 0; i < obj.text.length; ++i){
                    result.value.push({type: 'string', value: obj.text[i]})
                }
            } else {
                result.value.push({type: 'string', value: obj.text})
            }
        }
        res.json(result);
    })
};