const getQueryType = require('./getQueryType');
const insert = require('./insert');
const read = require('./read');

module.exports = function (req, res) {
    let uid, q;
    if (!(uid = req.query.user_id)) return res.json({
        result: false,
        error: 'User id must be specified'
    });
    if ((q = req.query.query) === undefined) return res.json({
        result: false,
        error: 'Query must be specified'
    });

    getQueryType(q, (err, type) => {
        if (err) return res.json({
            result: false,
            error: err.message
        });

        if (type == 'insert') {
            return insert(uid, q, res);
        } else {
            return read(uid, q, res);
        }
    });
};