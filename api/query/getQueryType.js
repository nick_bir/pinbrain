const utils = require('./utils');

function getQueryType(str, cb) {
    let phone = utils.searchPhone(str);
    let email = utils.searchEmail(str);
    let words = utils.searchWords(str);
    if (!words.length){
        if (phone || email) return cb(false, 'read');
        return cb(true);
    }
    if (phone || email){
        return cb(false, 'insert');
    }
    if (words.length < 3) {
        return cb(false, 'read');
    } else {
        return cb(false, 'insert');
    }
}

module.exports = getQueryType;