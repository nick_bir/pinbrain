const utils = require('./utils');
const model = require('../model');

module.exports = function (user_id, query, res) {
    let phone = utils.searchPhone(query);
    let email = utils.searchEmail(query);
    let words = utils.searchWords(query)
        .reduce((res, r) => r.length>3 ?
            res.concat(r.length<6 ? r : r.substr(0, r.length-2)) : res, []);

    model.findIdByText(user_id, words, (err, obj) => {
        if (err) return res.json({
            result: false,
            error: err.message
        });

        model.upsertData(user_id, obj, phone, email, query, err => {
            if (err) return res.json({
                result: true,
                error: err.message
            });
            res.json({result: true});
        });
    });
};