const getQueryType = require('./getQueryType');
const assert = require('assert');

function test_getQueryType() {
    let t = (val, exp_res) => getQueryType(val, (err, res) => assert(!err && (exp_res==res), val+', '+res));

    t('Vasya +79811568645', 'insert');
    t('Vasya', 'read');
    t('Vasya phone', 'read');
    t('Vasya vasya@mail.ru', 'insert');
    t('Vasya vasya@mail.ru +79811568645', 'insert');
    t('Vasya email', 'read');
    t('Vasya 8982563', 'insert');
    t('Vasya 8 (982) 563-52-40', 'insert');
    t('Vasya 8 (982) 563-52-40', 'insert');
    t('16541651', 'read');
    t('asdfa@dsfhgsr.rg', 'read');
    t('Василий', 'read');
    t('Пойти в лес в 12:00', 'insert');
    t('Буря мглою небо кроет', 'insert');

    console.log('done');
}

test_getQueryType();