'use strict';
const express = require('express');
const app = express();
const model = require('./model');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(express.static('public'))

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.get('/register', function (req, res) {
    if (!req.query.username || !req.query.password){
        return res.json({error: 'missing username or password', result: false});
    }
    let username = req.query.username.toLowerCase();
    let password = req.query.password.toLowerCase();
    return model.register(username, password, (err) => {
        if (err){
            console.log(err);
            return res.json({error: err, result: false});
        }
        return res.json({error: undefined, result: true});
    });
});

app.get('/login', function (req, res) {
    if (!req.query.username || !req.query.password){
        return res.json({error: 'missing username or password', result: false});
    }
    let username = req.query.username.toLowerCase();
    let password = req.query.password.toLowerCase();
    return model.login(username, password, (err, user_id) => {
        if (err){
            console.log(err);
            return res.json({error: err, result: false});
        }
        return res.json({error: undefined, result: !!user_id, user_id: user_id});
    });
});

app.get('/execute', require('./query'));

app.listen(3030, function () {
    console.log('API server listening on port 3030!');
});