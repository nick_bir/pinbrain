'use strict';

const BotStorage = require('./bot_storage');
const Telegram = require('telegram-node-bot');
const CheckAuthController = require('./router/checkAuth');
const BotController = require('./router/executeQuery');
const hub = require('./hub');

const tg = new Telegram.Telegram('294205514:AAE6j9a_YEbsqHVKNkf3G3MRitoGoNlyACA', {
    storage: new BotStorage()
});

const botController = new BotController();
const checkAuthController = new CheckAuthController();

tg.router
    .any(checkAuthController)
    .any(botController);