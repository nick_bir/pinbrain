const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;
const api = require('../api');
const hub = require('../hub');

class CheckAuthController extends TelegramBaseController {
    checkLogin($, username, password) {
        api('login', {username, password}, (err, res) => {
            if (err) return $.sendMessage('Server error occurred. Please, try your action later.');
            if (res.result) {
                hub[$.userId] = res.user_id;
                return $.setUserSession(''+$.userId, {user_id: res.user_id}).then(() =>
                    $.sendMessage('Authorization is successful.'));
            }
            api('register', {username, password}, (err, res) => {
                if (err) return $.sendMessage('Server error occurred. Please, try your action later.');
                if (res.result) {
                    hub[$.userId] = res.user_id;
                    return $.setUserSession(''+$.userId, {user_id: res.user_id}).then(() =>
                        $.sendMessage('Registration is successful.'));
                } else {
                    return $.sendMessage('Please, check your password or pick another username');
                }
            });
        });
    }

    getLogin($) {
        $.sendMessage(
            'Hello, I am PiNbrain bot!\n' +
            'You can use me for storing all information about your business. I will help you structure your ' +
            'data and find something you search. I can recognize some templates in storing data (like phone numbers ' +
            'and emails), and I provide useful ways to use your data. That\'s why my developers calls me "smart".\n');
        const form = {
            username: {
                q: 'Please, input your username to login or to register:',
                error: 'sorry, wrong username',
                validator: (message, callback) => {
                    if (!message.text) return callback(false);
                    callback(true, message.text);
                }
            },
            password: {
                q: 'Send me your password to login or register:',
                error: 'sorry, wrong password',
                validator: (message, callback) => {
                    if (!message.text) return callback(false);
                    callback(true, message.text);
                }
            }
        };
        $.runForm(form, (result) => {
            this.checkLogin($, result.username, result.password);
        });
    }

    handle($){
        if (hub[$.userId]) return;
        hub[$.userId] = 0;
        $.getUserSession(''+$.userId).then((user_data) => {
            hub[$.userId] = user_data;
            if (!user_data) {
                return this.getLogin($);
            }
        }).catch(err => {
            console.log(err);
            hub[$.userId] = undefined;
        });
    }
}

module.exports = CheckAuthController;