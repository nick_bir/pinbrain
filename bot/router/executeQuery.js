const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;
const api = require('../api');
const hub = require('../hub');
const util = require('util');

class BotController extends TelegramBaseController {
    handle($){
        if (hub[$.userId] === 0) return setTimeout(()=>{this.handle($)}, 1);
        if (!hub[$.userId]) return;
        api('execute', {"user_id": hub[$.userId], "query": $.message.text}, (err, res) => {
            if (err) return $.sendMessage('Server error occurred. Please, try again later.');
            if (!res.result) return $.sendMessage('Query error.');
            let value = res.value;
            if (!value) return $.sendMessage('Data saved successfully');
            if (value.length == 0){
                return $.sendMessage('Relevant data wasn\'t found');
            }
            for (let i = 0; i < value.length; ++i){
                if (value[i].type == 'string'){
                    $.sendMessage(value[i].value);
                } else if(value[i].type == 'link'){
                    $.sendMessage(util.format('[%s](%s)', value[i].text, value[i].url),{parse_mode: 'Markdown'});
                } else {
                    $.sendMessage('Unknown result type.');
                }
            }
        });
    }
}

module.exports = BotController;