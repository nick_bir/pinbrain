const api_url = 'http://api.pinbrain.ru/';
const request = require('request');

function apiRequest(method, params, cb) {
    request.get(api_url+method, {
        qs: params,
        json: true
    }, (err, response, result) => {
        cb(err, result);
    });
}

module.exports = apiRequest;