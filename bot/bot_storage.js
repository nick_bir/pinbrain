const BaseStorage = require('telegram-node-bot').BaseStorage;
const mongo = require('./db');
const async = require('async');

class BotStorage extends BaseStorage {
    get(storage, key) {
        return new Promise(function (resolve, reject) {
            let db = null;
            async.waterfall([
                cb => mongo(cb),
                (_db, cb) => {
                    db = _db;
                    db.collection(storage).findOne({key: key}, cb);
                }
            ], (err, db_data) => {
                if (db) db.close();
                if (err) {
                    reject(err);
                } else {
                    resolve(db_data ? db_data.data : undefined);
                }
            });
        });
    }

    set(storage, key, data) {
        return new Promise(function (resolve, reject) {
            let db = null;
            async.waterfall([
                cb => mongo(cb),
                (_db, cb) => {
                    db = _db;
                    db.collection(storage).updateOne({key: key},
                        {$set: {key: key, data: data}}, {upsert: true}, cb);
                }
            ], err => {
                if (db) db.close();
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    remove(storage, key) {
        return new Promise(function (resolve, reject) {
            let db = null;
            async.waterfall([
                cb => mongo(cb),
                (_db, cb) => {
                    db = _db;
                    db.collection(storage).removeOne({key: key}, cb);
                }
            ], err => {
                if (db) db.close();
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
}

module.exports = BotStorage;