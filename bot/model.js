'use strict';
const mongo = require('./db');
const async = require('async');

const collection_userInfo = 'user_info';

function getUserInfo(userId, callback) {
    let db = null;
    async.waterfall([
        cb => mongo(cb),
        (_db, cb) => {
            db = _db;
            db.collection(collection_userInfo).findOne({
                userId: userId
            }, cb);
        }
    ], (err, userInfo) => {
        if (db) db.close();
        callback(err, userInfo);
    });
}

function setUserInfo(userInfo, callback) {
    if (!userInfo.userId) {
        return callback(new Error('User id should be specified'));
    }
    let db = null;
    async.waterfall([
        cb => mongo(cb),
        (_db, cb) => {
            db = _db;
            db.collection(collection_userInfo).updateOne({
                userId: userInfo.userId
            }, {$set: userInfo}, {upsert: true}, cb);
        }
    ], err => {
        if (db) db.close();
        callback(err);
    });
}

module.exports = {
    getUserInfo,
    setUserInfo
};